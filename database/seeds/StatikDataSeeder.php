<?php

use Illuminate\Database\Seeder;

class StatikDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $type = ['cat', 'dog'];
        $user = \App\User::find(1);

        for ($i = 0; $i<50; $i++) {
            $customer = new \App\Customer([
                'firstname' => $faker->firstName,
                'created_at' => now(),
                'lastname' => $faker->lastName,
                'mobile_number' => $faker->phoneNumber,
            ]);

            $number = $faker->numberBetween(1,4);

            for ($k = 0; $k<$number; $k++) {
                $pet = new \App\Pet([
                    'name' => $faker->name,
                    'created_at' => now(),
                    'dob' => \Carbon\Carbon::createFromFormat('Y-m-d', $faker->date)->toDateTimeString(),
                    'chip_number' => $faker->creditCardNumber,
                    'type' => $faker->randomElement($type),
                    'weight' => $faker->randomFloat(2, 1, 100),
                ]);

                $user->customers()->save($customer);
                $customer->pets()->save($pet);
            }

        }

    }
}
