<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'Admin',
            'created_at' => now(),
            'email' => 'test@test.com',
            'email_verified_at' => now(),
            'password' => \Illuminate\Support\Facades\Hash::make('admin123'),
        ]);
    }
}
