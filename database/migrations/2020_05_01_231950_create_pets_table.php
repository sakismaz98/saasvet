<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name');
            $table->bigInteger('customer_id');
            $table->timestamp('dob')->nullable();
            $table->string('chip_number')->nullable()->unique();
            $table->enum('type', ['cat', 'dog']);
            $table->bigInteger('vaccine_id')->nullable();
            $table->float('weight', 3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
