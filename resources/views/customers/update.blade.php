@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Update/Edit Customer's Information
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                        @php
                            Session::forget('success')
                        @endphp
                    </div>
                @endif
            </div>
            <div class="card-body">
                <form action="{{ route('customers.update', $customer->id) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <h5>Customer's Information</h5>
                    <div class="form-group row">
                        <div class="col-3">
                            <label>Firstname</label>
                            <input type="text" name="firstname" class="form-control" value="{{ $customer->firstname }}">
                        </div>
                        <div class="col-3">
                            <label>Lastname</label>
                            <input type="text" name="lastname" class="form-control" value="{{ $customer->lastname }}">
                        </div>
                        <div class="col-3">
                            <label>Mobile Number</label>
                            <input type="text" name="mobile_number" class="form-control" value="{{ $customer->mobile_number }}">
                        </div>
                        <div class="col-3">
                            <label>Phone Number (Optional)</label>
                            <input type="text" name="phone_number" class="form-control" value="{{ $customer->phone_number }}">
                        </div>
                    </div>

                    <hr>

                    @foreach($customer->pets as $key => $pet)
                    <h5>Pet {{$key + 1}} information</h5>
                    <div class="form-group row">
                        <input type="hidden" name="pet_id[]" value="{{ $pet->id }}">

                        <div class="col-4">
                            <label>Pet's Name</label>
                            <input type="text" class="form-control" name="name[]" value="{{ $pet->name }}">
                        </div>

                        <div class="col-4">
                            <label>Date Of Birth</label>
                            <input type="date" class="form-control" name="dob[]" value="{{ $pet->dob->format('Y-m-d')  }}">
                        </div>

                        <div class="col-4">
                            <label>Pet Type</label>
                            <select name="type[]" class="form-control">
                                <option value="{{ $pet->type }}">{{$pet->type}}</option>
                                <option value="dog">Dog</option>
                                <option value="cat">Cat</option>
                            </select>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-4">
                            <label>Weight</label>
                            <input type="text" class="form-control" name="weight[]" value="{{ $pet->weight }}">
                        </div>
                        <div class="col-4">
                            <label>Chip Number</label>
                            <input type="text" class="form-control" name="chip_number[]" value="{{ $pet->chip_number }}">
                        </div>
                        <div class="col-4">
                            <label>Vaccine ID</label>
                            <input type="text" class="form-control" name="vaccine_id[]" value="{{ $pet->vaccine_id }}">
                        </div>
                    </div>
                    <hr>
                    @endforeach
                    <div class="form-group d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary mr-4">Save</button>
                        <button type="button" class="btn btn-secondary">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
