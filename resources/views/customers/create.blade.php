@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h4>Add New Customer</h4>
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                        @php
                            Session::forget('success')
                        @endphp
                    </div>
                @endif
            </div>
            <div class="card-body">
                <form action="{{ route('customers.save') }}" method="post">
                    @csrf
                    <h5>Customer's Information</h5>
                    <div class="form-group row">
                        <div class="col-3">
                            <label>Firstname</label>
                            <input type="text" name="firstname" class="form-control">
                        </div>
                        <div class="col-3">
                            <label>Lastname</label>
                            <input type="text" name="lastname" class="form-control">
                        </div>
                        <div class="col-3">
                            <label>Mobile Number</label>
                            <input type="text" name="mobile_number" class="form-control">
                        </div>
                        <div class="col-3">
                            <label>Phone Number (Optional)</label>
                            <input type="text" name="phone_number" class="form-control">
                        </div>
                    </div>

                    <h5>Pets information</h5>
                    <div class="form-group row">
                        <div class="col-4">
                            <label>Pet's Name</label>
                            <input type="text" class="form-control" name="name">
                        </div>

                        <div class="col-4">
                            <label>Date Of Birth</label>
                            <input type="date" class="form-control" name="dob">
                        </div>

                        <div class="col-4">
                            <label>Pet Type</label>
                            <select name="type" class="form-control">
                                <option value="dog">Dog</option>
                                <option value="cat">Cat</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-4">
                            <label>Weight</label>
                            <input type="text" class="form-control" name="weight">
                        </div>
                        <div class="col-4">
                            <label>Chip Number</label>
                            <input type="text" class="form-control" name="chip_number">
                        </div>
                        <div class="col-4">
                            <label>Vaccine ID</label>
                            <input type="text" class="form-control" name="vaccine_id">
                        </div>
                    </div>
                    <div class="form-group d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary mr-4">Save</button>
                        <button type="button" class="btn btn-secondary">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
