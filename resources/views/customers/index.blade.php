@extends('layouts.app')

@section('content')

    <div class="container-lg">
        <div class="row">
            <div class="col-6">
                <h2 class="h2">Manage customers</h2>
            </div>
            <div class="col-6">
                <a type="button" class="btn btn-primary" href="{{ route('customers.create') }}">Add New Customer</a>
            </div>
        </div>
        @if($customers->total() > 0)
        <h6 class="h6 py-2">{{ $customers->firstItem() + $customers->count() -1 }} of {{ $customers->total() }}</h6>
        @else
        <h6 class="h6 py-2">{{ $customers->firstItem() + $customers->count()  }} of {{ $customers->total() }}</h6>
        @endif
        <div class="row">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Mobile</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $customer)
                    <tr>
                        <td>{{ $customer->firstname }}</td>
                        <td>{{ $customer->lastname }}</td>
                        <td>{{ $customer->mobile_number }}</td>
                        <td>
                            <div class="form-inline d-flex justify-content-around">
                                <a type="button" href="{{ route('customers.edit', $customer->id) }}" class="btn btn-primary btn-md mx-2">Edit</a>
{{--                                <a href="{{ route('customers.show') }}" class="btn btn-secondary">More</a>--}}
                                <form action="{{ route('customers.delete', $customer->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger mx-2">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                {{ $customers->links() }}
            </div>
        </div>
    </div>

@endsection
