@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <a href="/create" class="btn btn-primary">Add Pet</a>
        </div>
        <div class="row justify-content-center py-2">
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Weight</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pets as $pet)
                        <tr>
                            <td>{{ $pet->type }}</td>
                            <td>{{ $pet->name }}</td>
                            <td>{{ $pet->weight }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success')
                @endphp
            </div>
        @endif
    </div>
@endsection
