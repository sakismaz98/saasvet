@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <form action="{{ route('pet.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Pet Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Pet's Name">
                </div>
                <div class="form-group">
                    <label for="type">Type</label>
                    <select name="type" class="form-control">
                        <option value="dog">Dog</option>
                        <option value="cat">Cat</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="weight">Weight</label>
                    <input type="number" name="weight" placeholder="Pet's weight" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
