<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main.welcomeApp');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth', 'verified']], function () {

    Route::get('/dashboard', 'HomeController@index')->name('home');

    Route::get('/pets/index', 'PetController@index');
    Route::get('/pets/create', 'PetController@create');
    Route::post('/pets/create', 'PetController@store')->name('pet.store');

    Route::get('/customer/manage', 'CustomerController@index')->name('customers.manage');
    Route::get('/customer/manage/create', 'CustomerController@create')->name('customers.create');
    Route::post('/customer/manage/create', 'CustomerController@store')->name('customers.save');
    Route::get('/customer/manage/{id}', 'CustomerController@show')->name('customers.show');
    Route::get('/customer/manage/{id}/edit', 'CustomerController@edit')->name('customers.edit');
    Route::patch('/customer/manage/{id}', 'CustomerController@update')->name('customers.update');
    Route::delete('/customer/manage/{id}', 'CustomerController@destroy')->name('customers.delete');

});



