<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pet extends Model
{
    use SoftDeletes;

    protected $dates = [
        'dob'
    ];

    protected $fillable = [
        'name',
        'type',
        'weight',
        'dob',
        'chip_number',
        'vaccine_id'
    ];

    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
