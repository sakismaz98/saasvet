<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    protected $fillable = [
      'firstname',
      'lastname',
      'mobile_number',
      'phone_number',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pets()
    {
        return $this->hasMany('App\Pet');
    }

}
