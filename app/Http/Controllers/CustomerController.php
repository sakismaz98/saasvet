<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Pet;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $customers = \auth()->user()->customers()->with('pets')->orderBy('id')->paginate(15);
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'mobile_number' => 'required|numeric',
            'phone_number' => 'nullable|numeric0'
        ]);

        $customer = new Customer([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'mobile_number' => $request->input('mobile_number'),
            'phone_number' => $request->input('phone_number')
        ]);

        $pet = new Pet([
            'name' => $request->input('name'),
            'dob' => Carbon::createFromDate($request->input('dob')),
            'type' => $request->input('type'),
            'weight' => $request->input('weight'),
            'chip_number' => $request->input('chip_number'),
            'vaccine_id' => $request->input('vaccine_id')
        ]);

        $user->customers()->save($customer);

        $customer->pets()->save($pet);

        return back()->with('success', 'New Customer Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // TODO: test the best approach
        // $customer = auth()->user()->customers()->where('id', $id)->first();
        $customer = Customer::find($id);

        return view('customers.update', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateCustomer = $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'mobile_number' => 'required',
            'phone_number' => 'nullable'
        ]);

        // TODO: refactor to one liner if possible
        $customer = auth()->user()->customers()->where('id', $id)->first();
        $customer->update($updateCustomer);

        $pets_ids = $request->pet_id;

        foreach ($pets_ids as $key => $id) {
            $pet = Pet::find($id);
            $pet->name = $request->name[$key];
            $pet->dob = $request->dob[$key];
            $pet->type = $request->type[$key];
            $pet->chip_number = $request->chip_number[$key];
            $pet->weight = $request->weight[$key];
            $pet->vaccine_id = $request->vaccine_id[$key];
            $pet->save();
        }

        return back()->with(['success' => 'Successfully Updated Records']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        auth()->user()->customers()->where('id', $id)->update(['deleted_at' => now()]);

        return redirect()->back();
    }
}
