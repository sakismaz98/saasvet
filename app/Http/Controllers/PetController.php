<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PetController extends Controller
{
    public function index(\App\User $user)
    {
        $pets = (auth()->user()) ? auth()->user()->pets : false;

        return view('pets.index', compact('pets'));
    }

    public function create()
    {
        return view('pets.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['nullable','string'],
            'weight' => ['required', 'numeric'],
            'type' => 'required'
        ]);

        auth()->user()->pets()->create([
            'name' => $data['name'],
            'weight' => $data['weight'],
            'type' => $data['type'],
        ]);

        return redirect('/index')->with('success', 'New Pet Add Successfully');
    }
}
